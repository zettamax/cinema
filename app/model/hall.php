<?

class Model_Hall extends Model
{

    public function getAllSeatsByHallId($id) // get Seats in rows by hall id
    {
        $rowStatement = $this->getPDO()->prepare(
        'SELECT `hall`.`id`   AS `id`,
            `hall`.`name` AS `name`,
            `row`.`num`   AS `row_num`,
            `row`.`id`    AS `row_id`
        FROM   `hall`
          LEFT JOIN `row`
               ON `hall`.`id` = `row`.`hall_id`
        WHERE  `hall`.`id` = :id;'
        );
        $rowStatement->setFetchMode(PDO::FETCH_ASSOC);
        $rowStatement->execute(array('id' => $id)); // get rows by hall id
        $rowsArray = $rowStatement->fetchAll();
        $rowsIdsArray = array();
        foreach($rowsArray as $row) {
            $rowsIdsArray[] = $row['row_id'];
        }
        $rowsIdsPrepare = implode(',', array_fill(0, count($rowsIdsArray), '?'));
        $seatStatement = $this->getPDO()->prepare(
            "SELECT
                `seat`.`row_id` AS `row_id`,
                `seat`.`id` AS `seat_id`,
                `seat`.`num` AS `seat_num`,
                `order`.`fullname` AS `fullname`
            FROM `seat`
               LEFT OUTER JOIN `order`
                ON `order`.`seat_id` = `seat`.`id`
            WHERE `seat`.`row_id` IN ($rowsIdsPrepare);"
        );
        $seatStatement->setFetchMode(PDO::FETCH_ASSOC);
        $seatStatement->execute($rowsIdsArray); // get seats by rows id
        $seatsArray = $seatStatement->fetchAll();
        foreach ($rowsArray as &$row) { // add seats to rows array by row id
            foreach ($seatsArray as $seat) {
                if ($row['row_id'] == $seat['row_id']) {
                    $row['seats'][] = $seat;
                }
            }
        }
        return $rowsArray;
    }

    public function orderSeatById($seat_id, $fullname) // add record about order to order table by seat id
    {
        $orderStatement = $this->getPDO()->prepare('INSERT INTO `order` (`fullname`, `seat_id`) VALUES (:fullname, :seat_id)');
        $orderStatement->setFetchMode(PDO::FETCH_ASSOC);
        $orderStatement->execute(array('fullname' => $fullname, 'seat_id' => $seat_id));
    }

}