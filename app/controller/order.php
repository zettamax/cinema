<?

class Controller_Order
{

    public function action_index()
    {
        $hall = Model::factory('hall');
        $hallRows = $hall->getAllSeatsByHallId(1); // get rows and seats from first hall
        $smarty = new Smarty();
        $smarty->debugging = true;
        $smarty->assign('rows', $hallRows);
        $smarty->display('order.tpl'); // display page
    }

    public function action_order()
    {
        $seat_id = $_POST['seat_id']; // get vars from POST
        $fullname = $_POST['fullname'];
        if ($seat_id && $fullname) { // stupid validation
            $hall = Model::factory('hall');
            $hall->orderSeatById($seat_id, $fullname); // order free seat
        }
        header('Location: /order'); // redirect to main page
    }

}