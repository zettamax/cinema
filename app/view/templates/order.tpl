<h2>Cinema order system <small>v0.1</small></h2>

<table>
{foreach from=$rows item=row}
    <tr>
    {foreach from=$row.seats item=seat}
    <td>
        <form action="/order/order" method="post">
            <input type="hidden" name="seat_id" value="{$seat.seat_id}">
            <input type="text" name="fullname"
            {if isset($seat.fullname)}
            value="{$seat.fullname}" readonly="readonly" disabled="disabled"
            {/if}
            >
            <button
            {if isset($seat.fullname)}
            disabled="disabled"
            {/if}
            >
            Row {$row.row_num}
            <br>
            Seat {$seat.seat_num}
            </button>
        </form>
    </td>
    {/foreach}
    </tr>
{/foreach}
</table>