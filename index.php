<?

define('SITE_PATH', dirname(__FILE__)); // constants
define('APP_PATH', SITE_PATH.'/app/');
define('SYS_PATH', SITE_PATH.'/sys/');
define('CORE_PATH', SITE_PATH.'/sys/core/');
include CORE_PATH.'init.php'; // init file with autoload, DB init, etc

$router = new Router; // start processing request
$router->delegate();