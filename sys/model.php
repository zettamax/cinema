<?

abstract class Model {

    /**
     * @var PDO $pdo_instance
     */
    protected static $pdo_instance;
    /**
     * @var PDO $pdo
     */
    protected $pdo;

    public static function factory($name) // return concrete model with PDO instance
    {
        self::checkPDO();
        $name = "Model_$name";
        $model = new $name;
        $model->setPDO(self::$pdo_instance);
        return $model;
    }

    private static function checkPDO() // check PDO object
    {
        if (!self::$pdo_instance instanceof PDO) {
            throw new Exception("Can't find PDO connection, use Model::init(dsn, user, pass)");
        }
    }

    public static function init($dsn, $user, $pass) // init PDO with DSN, user, pass
    {
        if (!self::$pdo_instance) {
            self::$pdo_instance = new PDO($dsn, $user, $pass);
            self::initFillDB(); // fill DB with test data
        }
    }

    public static function initFillDB() // fill test data to DB
    {
        $rows = 5;
        $seats = 4;
        $pdo = self::$pdo_instance;
        $pdo->exec('CREATE TABLE IF NOT EXISTS `hall` (`id` INT AUTO_INCREMENT PRIMARY KEY, `name` VARCHAR(20))');
        $countStatement = $pdo->query('SELECT COUNT(*) AS count FROM hall');
        $countStatement->setFetchMode(PDO::FETCH_ASSOC);
        $count = $countStatement->fetch();
        if (!$count['count']) {
//            droping all existing tables if table hall empty
            $pdo->exec('DROP TABLE IF EXISTS `hall`');
            $pdo->exec('DROP TABLE IF EXISTS `row`');
            $pdo->exec('DROP TABLE IF EXISTS `seat`');
            $pdo->exec('DROP TABLE IF EXISTS `order`');
//            create and fill table hall
            $pdo->exec('CREATE TABLE IF NOT EXISTS `hall` (`id` INT AUTO_INCREMENT PRIMARY KEY, `name` VARCHAR(20))');
            $pdo->exec('INSERT INTO `hall` (`name`) VALUES ("Aquamarine")');
            $hall_id = $pdo->lastInsertId();
//            create table row
            $pdo->exec('CREATE TABLE IF NOT EXISTS `row` (`id` INT AUTO_INCREMENT PRIMARY KEY, `hall_id` INT, `num` INT)');
//            create table seat
            $pdo->exec('CREATE TABLE IF NOT EXISTS `seat` (`id` INT AUTO_INCREMENT PRIMARY KEY, `row_id` INT, `num` INT)');
            for($i = 0; $i < $rows; $i++) {
//                fill table row
                $num1 = $i + 1;
                $pdo->exec("INSERT INTO `row` (`hall_id`, `num`) VALUES ($hall_id, $num1)");
                $row_id = $pdo->lastInsertId();
                for($j = 0; $j < $seats; $j++) {
//                    fill table seats
                    $num2 = $j + 1;
                    $pdo->exec("INSERT INTO `seat` (`row_id`, `num`) VALUES ($row_id, $num2)");
                }
            }
//            create table order
            $pdo->exec('CREATE TABLE IF NOT EXISTS `order` (`id` INT AUTO_INCREMENT PRIMARY KEY, `fullname` VARCHAR(20), `seat_id` INT)');
            $pdo->exec("INSERT INTO `order` (`fullname`, `seat_id`) VALUES ('Vasily Pupkin', 2)");
        }
    }

    public function getPDO()
    {
        return $this->pdo;
    }

    public function setPDO($pdo)
    {
        $this->pdo = $pdo;
    }

}