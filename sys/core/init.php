<?

include CORE_PATH.'autoload.php';
include APP_PATH.'db_config.php';
Autoload::register(); // register autoload function
Model::init('mysql:dbname='.$db_config['dbname'].';host='.$db_config['host'], $db_config['user'], $db_config['pass']); // init PDO in model

define('SMARTY_DIR', SITE_PATH.'/smarty/libs/');
include(SMARTY_DIR.'Smarty.class.php'); // load Smarty class