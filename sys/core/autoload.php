<?

class Autoload
{

    public static function register()
    {
        if (!spl_autoload_register('Autoload::load'))
        {
            throw new Exception("Can't register autoload function");
        }
    }

    public static function load($class) // simple cascading file system :) kohana-style
    {
        $autoload_dirs = array(APP_PATH, SYS_PATH, CORE_PATH); //dirs: /app, /sys, /sys/core
        for ($i = 0; $i < count($autoload_dirs); $i++) {
            $class_file = $autoload_dirs[$i].self::get_file_by_class($class).'.php';
            if (file_exists($class_file)) {
                include $class_file;
                if (class_exists($class)) break;
            }
        }
    }

    private static function get_file_by_class($class_name)
    {
        return str_replace('_', '/', strtolower($class_name));
    }

}