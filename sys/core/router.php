<?

class Router
{
    private $controller;
    private $action;
    private $params;

    private function get_controller() // get controller and action or set default
    {
        $params = explode('/', $_GET['q']);
        $controller = array_shift($params);
        $this->controller = $controller ? $controller : 'order';
        $action = array_shift($params);
        $this->action = $action ? $action : 'index';
        $this->params = $params;
    }

    private function start_controller() // invoke controller and method
    {
        $controller_name = 'Controller_' . $this->controller;
        $action_name = 'action_' . $this->action;
        $controller = new $controller_name;
        $controller->$action_name();
    }

    public function delegate() // start!!!
    {
        $this->get_controller();
        $this->start_controller();
    }

}